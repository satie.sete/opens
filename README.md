# Project opENS
## Open Smart-Grid project from ENS Rennes and SATIE-CNRS laboratory
![Welcome_Project_opENS](Pictures/Welcome_Project_opENS.png)

### Présentation du Project opENS 
L'étude de Smart Grid est pluridisciplinaire et est caractérisée par des difficultés expérimentales liées aux disciplines, aux coûts, aux données disponibles et à la complexité des systèmes étudiés. Cet article présente le concept opENS, basé sur un démonstrateur de réseau faible coût, libre et ouvert à la communauté. Il tend à faciliter certains axes de recherche liée au Smart Grid en rendant possible la mise à l'échelle d'un grand nombre d'acteurs dans les réseaux. Nommés "noeuds opENS", les pièces maîtresses de ce concept sont présentées : des onduleurs triphasés avec leurs commandes et intelligences individuelles pour émuler des agents réseaux, dans un environnement instrumenté et supervisé. Ces noeuds embarquent 3 couches matérielles configurables : i) onduleur triphasé, ii) commande rapprochée et asservissements numériques, iii) émulation, supervision et interface réseau. Smart Grid, réseaux électriques, plateforme expérimentale, données libres.

Pour en savoir : [Article SGE - 2020](https://theses.hal.science/ENS-RENNES-MECATRONIQUE/hal-03099423v1)

### Architecture du GitLab

- [X] [Hardware Development](https://gitlab.com/satie.sete/opens/-/tree/hardware_development)
- [X] [Node_opENS_Raspberry](https://gitlab.com/satie.sete/opens/-/tree/Node_opENS_Raspberry)
- [X] [Node_opENS_DSP](https://gitlab.com/satie.sete/opens/-/tree/Node_opENS_DSP)
- [X] [Server Maman](https://gitlab.com/satie.sete/opens/-/tree/Server_Maman)

A noter : *Afin de simplifier la maintenance de l'ensemble des briques du projet, nous avons choisi de décomposer notre référentiel Git en plusieurs branches, chacune étant dédiée à un bloc spécifique. Cette approche vise à faciliter les mises à jour et à prévenir l'encombrement des fichiers inutiles sur les machines. Ainsi, seuls les éléments nécessaires sont présents, favorisant une gestion efficace et épurée de votre environnement de développement.*

### Documentation, à la découverte des fonctionnalités
#### [Wiki Français](https://gitlab.com/gusj/opens/-/wikis/home) / [Wiki English ](https://gitlab.com/satie.sete/opens/-/wikis/HomeEN#)

*Afin de découvrir l'ensemble des fonctionnalités et le potentiel du projet, vous retrouverez ci-dessous une documentation des différentes possibilités.*

#### SOMMAIRE
**Architecture**
- [Réseau - Protocole - Répartition - Gestion de données](https://gitlab.com/satie.sete/opens/-/wikis/Architecture)

**DSP**
- [Variable environnement](https://gitlab.com/satie.sete/opens/-/wikis/Documentation#variable-environnement)
- [Grid Feeding](https://gitlab.com/satie.sete/opens/-/wikis/Documentation#grid-feeding)
- [Grid Forming](https://gitlab.com/satie.sete/opens/-/wikis/Documentation#grid-forming)

**Raspberry**
- [Tuto de lancement](https://gitlab.com/satie.sete/opens/-/wikis/Documentation#tuto-de-lancement)
- [Contrôle à distance d'un noeud](https://gitlab.com/satie.sete/opens/-/wikis/Documentation#contrôle-à-distance-dun-noeud)
- [Screen DUO](https://gitlab.com/satie.sete/opens/-/wikis/Documentation#screen-duo)
- [Gestion globale du réseau](https://gitlab.com/satie.sete/opens/-/wikis/Documentation#gestion-globale-du-réseau)
- [Ajouter un code client](https://gitlab.com/satie.sete/opens/-/wikis/Documentation#ajouter-un-code-client)
- [Arret d'un noeud](https://gitlab.com/satie.sete/opens/-/wikis/Documentation#arrêt-dun-noeud)
- [Téléchargement des graphiques](https://gitlab.com/satie.sete/opens/-/wikis/Documentation#téléchargement-des-graphiques)
- [Gestion INFORMATION des noeuds](https://gitlab.com/satie.sete/opens/-/wikis/Documentation#gestion-information-des-noeuds)
- [Gestion des LEDs RGB des noeuds](https://gitlab.com/satie.sete/opens/-/wikis/Documentation#gestion-des-leds-rgb-des-noeuds)

**Serveur Maman**
- Chronograph
  - [Utilisation](https://gitlab.com/satie.sete/opens/-/wikis/Documentation#utilisation)
  - [Création de Dashboards](https://gitlab.com/satie.sete/opens/-/wikis/Documentation#création-de-dashboards)
  - [Utilisation des variables, gestion temps](https://gitlab.com/satie.sete/opens/-/wikis/Documentation#utilisation-des-variables-gestion-temps)
  - [Téléchargement d'une expérimentation (CSV)](https://gitlab.com/satie.sete/opens/-/wikis/Documentation#téléchargement-dune-expérimentation-csv)

### Auteurs
*Ordre alphabétique*
ENS Rennes & SATIE : Hamid BEN AHMED, Roman LE GOFF LATIMIER, Gurvan JODIN

Stagiaires talentueux : Salim ABDOU DAOURA, Yoann BARON, Théo CHATEL, Jerôme KERIBIN, Thomas LESAULNIER, Julien LEYRIT, Wenyi LI, Carl LYONNARD, Quentin MAHÉ, Rachel NEVEU, Rabah OUALI, Alexis PICOT.

### LICENSE
*Les licences des différents codes et plans sont détaillés dans les dossiers concernés.*

Copyright © ENS Rennes, SATIE CNRS UMR 8029

Permission is hereby granted, free of charge, to any person obtaining a copy of this software, drawings and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
